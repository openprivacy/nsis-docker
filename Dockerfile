FROM cirrusci/android-sdk:tools-windowsservercore-2019

# Install signtool
ADD https://github.com/microsoft/MSIX-Toolkit/releases/download/1.4/MSIX-Toolkit.x64.zip C:/TEMP/MSIX-Toolkit.x64.zip
RUN powershell -command "Expand-Archive -Path C:\TEMP\MSIX-Toolkit.x64.zip -DestinationPath C:\\"

# Install nsis
ADD https://sourceforge.net/projects/nsis/files/NSIS%203/3.06.1/nsis-3.06.1.zip/download C:/TEMP/nsis.zip
RUN powershell -command "Expand-Archive -Path C:\TEMP\nsis.zip -DestinationPath C:\\"
RUN move C:\nsis-3.06.1 C:\nsis

RUN setx path "%path%;C:\MSIX-Toolkit.x64\;C:\nsis\bin"
