# openpriv/nsis-docker

A Docker container provided by [Open Privacy](https://openprivacy.ca/) that is based on Windows Server Core 2019 and has MSIX-Toolkit for `signtool` and NSIS 3 for `makensis`, both in `%PATH%`

Dockerfile @ [git.openprivacy.ca/openprivacy/nsis-docker](https://git.openprivacy.ca/openprivacy/nsis-docker)
